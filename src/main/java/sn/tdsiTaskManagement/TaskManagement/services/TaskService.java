package sn.tdsiTaskManagement.TaskManagement.services;


import java.util.List;


import sn.tdsiTaskManagement.TaskManagement.entity.Task;

/**
 * Task
 */

public interface TaskService {

   List<Task> getAllTasks();
    
} 
package sn.tdsiTaskManagement.TaskManagement.controller;

import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import sn.tdsiTaskManagement.TaskManagement.entity.Task;
import sn.tdsiTaskManagement.TaskManagement.services.TaskService;

import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;




@RestController
@RequestMapping("api/v1/task")
@RequiredArgsConstructor
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping
    public List<Task> getAllTasks() {
        return this.taskService.getAllTasks();
    }
    
}

package sn.tdsiTaskManagement.TaskManagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sn.tdsiTaskManagement.TaskManagement.entity.Task;

/**
 * TaskRepository
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    
}
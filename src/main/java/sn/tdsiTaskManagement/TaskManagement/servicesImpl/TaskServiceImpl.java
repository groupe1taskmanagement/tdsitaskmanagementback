package sn.tdsiTaskManagement.TaskManagement.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.tdsiTaskManagement.TaskManagement.entity.Task;
import sn.tdsiTaskManagement.TaskManagement.repository.TaskRepository;
import sn.tdsiTaskManagement.TaskManagement.services.TaskService;

@Service
public class TaskServiceImpl implements TaskService {
@Autowired
    private TaskRepository repository;

    public TaskServiceImpl(TaskRepository repository){
        this.repository  = repository;
    }

    @Override
    public List<Task> getAllTasks() {
        return repository.findAll();
    }

   

    
}
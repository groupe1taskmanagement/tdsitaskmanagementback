package sn.tdsiTaskManagement.TaskManagement.entity;
import java.time.LocalDate;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity(name = "task")
public class Task {
    
    @Id
    @SequenceGenerator(
            name = "task_id_sequence",
            sequenceName = "task_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "task_id_sequence"
    )
    private Integer id;
    private String description;
    private boolean etat;
    private LocalDate dateCreation;

    // public Integer getId() {
    //     return id;
    // }

    // public void setId(Integer id) {
    //     this.id = id;
    // }

    // public String getDescription() {
    //     return description;
    // }

    // public void setDescription(String description) {
    //     this.description = description;
    // }

    // public boolean isEtat() {
    //     return etat;
    // }

    // public void setEtat(boolean etat) {
    //     this.etat = etat;
    // }

    // public LocalDate getDateCreation() {
    //     return dateCreation;
    // }

    // public void setDateCreation(LocalDate dateCreation) {
    //     this.dateCreation = dateCreation;
    // }

    // public Task() {
    // }

    // public Task(Integer id, String description, boolean etat, LocalDate dateCreation) {
    //     this.id = id;
    //     this.description = description;
    //     this.etat = etat;
    //     this.dateCreation = dateCreation;
    // }
}

